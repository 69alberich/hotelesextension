<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;


class AddBookingOptionField extends Migration
{
    const TABLE_NAME = 'hesperiaplugins_hoteles_hotel';
    
    public function up()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'minimo_noches') 
            || Schema::hasColumn(self::TABLE_NAME, 'noches_antelacion')) {
            return;
        }
        
        Schema::table(self::TABLE_NAME, function (Blueprint $obTable){
            $obTable->integer('minimo_noches')->default(0);
            $obTable->integer('noches_antelacion')->default(0);
        });
    }

    public function down(){
        
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'minimo_noches')
            || !Schema::hasColumn(self::TABLE_NAME, 'noches_antelacion')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('minimo_noches');
            $obTable->dropColumn('noches_antelacion');
    
        });
        // Schema::drop('qchsoft_hotelesextension_table');
    }
}
