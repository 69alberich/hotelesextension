<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftHotelesextensionPlaceables extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_hotelesextension_placeables', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('place_id');
            $table->integer('placeable_id');
            $table->string('placeable_type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_hotelesextension_placeables');
    }
}
