<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftHotelesextensionCategories extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_hotelesextension_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 150);
            $table->string('slug', 150);
            $table->string('external_id', 50);
            $table->text('description');
            $table->text('preview_text');
            $table->integer('active')->default(0);
            $table->string('code');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_hotelesextension_categories');
    }
}
