<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftHotelesextensionPlaces extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_hotelesextension_places', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 150);
            $table->text('description');
            $table->string('external_id', 150);
            $table->string('slug', 150);
            $table->string('latitude', 150);
            $table->string('longitude', 150);
            $table->integer('city_id');
            $table->text('synonymous');
            $table->text('preview_text');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_hotelesextension_places');
    }
}
