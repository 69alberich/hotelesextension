<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftHotelesextensionHotelExtraCategories extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_hotelesextension_hotel_extra_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('hotel_id');
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_hotelesextension_hotel_extra_categories');
    }
}
