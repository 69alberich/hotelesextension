<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;


class AddStarsSortFields extends Migration
{
    const TABLE_NAME = 'hesperiaplugins_hoteles_hotel';
    
    public function up()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'orden') 
            || Schema::hasColumn(self::TABLE_NAME, 'estrellas')) {
            return;
        }
        
        Schema::table(self::TABLE_NAME, function (Blueprint $obTable){
            $obTable->integer('orden')->default(0);
            $obTable->integer('estrellas')->default(0);
        });
    }

    public function down(){
        
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'orden')
            || !Schema::hasColumn(self::TABLE_NAME, 'estrellas')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('orden');
            $obTable->dropColumn('estrellas');
    
        });
        // Schema::drop('qchsoft_hotelesextension_table');
    }
}
