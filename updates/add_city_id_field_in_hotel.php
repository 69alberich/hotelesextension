<?php namespace Qchsoft\HotelesExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;


class AddCityIdField extends Migration
{
    const TABLE_NAME = 'hesperiaplugins_hoteles_hotel';
    
    public function up()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'city_id')) {
            return;
        }
        
        Schema::table(self::TABLE_NAME, function (Blueprint $obTable){
            $obTable->integer('city_id')->default(0);
        });
    }

    public function down(){
        
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'city_id')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('city_id');
    
        });
        // Schema::drop('qchsoft_hotelesextension_table');
    }
}
