document.addEventListener('DOMContentLoaded', function() {

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      locale: 'es',
      height: '100%',
      expandRows: true,
      slotMinTime: '08:00',
      slotMaxTime: '20:00',
      headerToolbar: false,
      initialView: 'dayGridMonth',
      initialDate: new Date,
      //navLinks: true,  can click day/week names to navigate views
      editable: false,
      selectable: true,
      nowIndicator: true,
      dayMaxEvents: true, // allow "more" link when too many events

      select: function(info) {
        //alert('selected ' + info.startStr + ' to ' + info.endStr);
        loadModalPrices(info);

      },

      selectOverlap: function(event) {

        return event.extendedProps.quantity > 0 ;
        //return event.rendering === 'background';
      }

    });

    calendar.render();
    loadEvents(calendar, 1);

    document.getElementById('prev').addEventListener('click', function() {
      calendar.prev(); // call method
      loadEvents(calendar, 1);
    });

    document.getElementById('next').addEventListener('click', function() {
      calendar.next(); // call method

      loadEvents(calendar, 1);
      //console.log(calendar.view.activeStart);
     // console.log(calendar.view.activeEnd);
    });

      function loadEvents(calendar, id){

        const view = calendar.view;
        const curDate = calendar.getDate();
        const month = curDate.toLocaleString('default', { month: 'long', year: 'numeric'});

        $("#month-label").text(month);

        $.request('onLoadDateQuantity', {
          data: {
            id: id,
            startDate: view.activeStart.toLocaleDateString().replaceAll("/", "-"),
            endDate: view.activeEnd.toLocaleDateString().replaceAll("/", "-")
          },
          success: function(response) {
            console.log(response);
            //this.success(response);
            if(response.status == "success"){
              renderEvents(response.value);
            }
            
          },
        });
      }

      function loadModalPrices(info){
        let startDate = moment(info.startStr);
        let endDate = moment(info.endStr);
        //endDate.subtract("1", "day"); 
        $.request('onLoadDatePrices', {
          data: {
            startDate: startDate.format("DD-MM-YYYY"),
            endDate: endDate.format("DD-MM-YYYY")
          },
          success: function(response) {
            console.log(response);
            this.success(response);
            
            $("#contentBasic").modal("show");
            
          },
        });
      }

      $("#contentBasic").on("hide.bs.modal", function(e){
    
        console.log(calendar);
        calendar.updateSize()
      });


      function renderEvents(arrayEvent){
        var events = arrayEvent.map(function(date){
            var event = {}
            event["title"] = date.quantity+" Disponibles";
            event["start"] = date.date;
            event["display"] = "list-item";

            if (date.quantity == 0) {
              event["display"] = "list-item";
              event["backgroundColor"] = "#e35d6a";
              event["classNames"] = [
                "event-danger"
              ]
              
            }
            event["extendedProps"] = {
              'quantity' : date.quantity
            }

            return event;
        });
        
        var eventSources = calendar.getEventSources();
        eventSources.forEach(eventSource => {
          eventSource.remove();
        });

        calendar.addEventSource(events);
       
      }


  });