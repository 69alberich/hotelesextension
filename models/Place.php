<?php namespace Qchsoft\HotelesExtension\Models;

use QchSoft\Location\Models\City;
use HesperiaPlugins\Hoteles\Models\Hoteles;
use Model;

/**
 * Model
 */
class Place extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_hotelesextension_places';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        "city" => [City::class, "key" => "city_id"]
    ];

    public $morphedByMany = [
        'hotels'  => [Hoteles::class, 'name' => 'placeable',
        'table' => 'qchsoft_hotelesextension_placeables'],
    ];
}

