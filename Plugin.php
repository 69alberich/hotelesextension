<?php namespace Qchsoft\HotelesExtension;

use System\Classes\PluginBase;
use QchSoft\HotelesExtension\Classes\Event\HotelModelHandler;
use QchSoft\HotelesExtension\Classes\Event\HotelControllerHandler;
use QchSoft\HotelesExtension\Classes\Event\HabitacionesControllerHandler;

use Event;
use Backend;

class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'Qchsoft\HotelesExtension\Components\SearchHandler' => 'SearchHandler',
            'Qchsoft\HotelesExtension\Components\CurrencyHandler' => 'CurrencyHandler',
            'Qchsoft\HotelesExtension\Components\HotelPage' => 'HotelPage',
            'Qchsoft\HotelesExtension\Components\TourPage' => 'TourPage',
            'Qchsoft\HotelesExtension\Components\UpsellingPage' => 'UpsellingPage',
            'Qchsoft\HotelesExtension\Components\CartHandler' => 'CartHandler',
            'Qchsoft\HotelesExtension\Components\PaymentMethodsHandler' => 'PaymentMethodsHandler',
            'Qchsoft\HotelesExtension\Components\ReservationPage' => 'ReservationPage',
            'Qchsoft\HotelesExtension\Components\PurchasePage' => 'PurchasePage',
          ];
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'Hoteles Extension Config',
                'icon'        => 'icon-building-o',
                'description' => '',
                'class'       => 'QchSoft\HotelesExtension\Models\Settings',
                'order'       => 600,
            ]
        ];
    }

    public function boot(){
        
        
        $this->addEventListener();

        
    }

    protected function addEventListener(){

        Event::subscribe(HotelModelHandler::class);
        Event::subscribe(HotelControllerHandler::class);
        Event::subscribe(HabitacionesControllerHandler::class);

    }
}
