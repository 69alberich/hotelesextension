<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use Db;
use HesperiaPlugins\Hoteles\Models\Fecha;
use Carbon\Carbon;
use October\Rain\Support\Collection;
use Qchsoft\HotelesExtension\Classes\Item\HotelResult;

class SearchHelper {

    const PLACES = "Lugares";
    const CITIES = "Ciudades";
    const HOTEL = "Hoteles";


    public static function getSuggestions(){
        $places = Db::table("qchsoft_hotelesextension_places")
        ->select("id",'name as value', Db::raw("'".self::PLACES."' as icon"));
        
        $cities = Db::table("qchsoft_location_cities")
        ->select("id",'name as value', Db::raw("'".self::CITIES."' as icon"));

        $hoteles = Db::table("hesperiaplugins_hoteles_hotel")
        ->select("id",'nombre as value', Db::raw("'".self::HOTEL."' as icon"))
        ->union($cities)
        ->union($places)->get();

        return $hoteles->toArray();
        
    }


    public static function getAvailableRooms($params){
        
        $data = self::prepareData($params);

        $result = self::searchForAvailability($data);
        $arRoomId = self::getIdsFromResult($result, $data, "room");

        return $arRoomId;

    }

    public static function getAvailableHotels($params){

      $arResult = [
        "list"=> null,
        "filters"=> null
      ];
      
      $formattedParams = self::prepareData($params);

      if ($formattedParams != null) {
        $result = self::searchForAvailability($formattedParams);
     
        $arHotelId = self::getIdsFromResult($result, $formattedParams, "hotel");
        
        $arHotelResult = array();

        for ($i=0; $i < count($arHotelId) ; $i++) { 
            
          $arHotelResult[$i] = new HotelResult($arHotelId[$i], $result, $formattedParams);
          
        }
        $collection = new Collection($arHotelResult);
        
        $arResult["list"] = $collection;
        $arResult["filters"] = self::getFilters($collection);
      }
      
      return $arResult;
    }

    protected static function getFilters($collection){

      $arFilters = array();
      $arStars = array();
      $arCategories = array();

      foreach ($collection as $hotel) {
        
        $arAttributes = $hotel->getAttributes();
        
        if (!in_array($arAttributes["estrellas"], $arStars)) {

          array_push($arStars, $arAttributes["estrellas"] );

        }

        if (!in_array($hotel->category->id, $arCategories)) {
          $arCategories[$hotel->category->id] = $hotel->category->name;
        }
       
      }
      
      $arFilters["stars"] = $arStars;
      $arFilters["categories"] = $arCategories;
      
      return $arFilters;
    }

    public static function prepareData($params){
        $data = null;

        if (isset($params["dates"])) {
            $arDates = explode(" to ", $params["dates"]);
            $data["checkin"] = $arDates[0];
            $data["checkout"] = $arDates[1];
        }else{
          return null;
        }

        if (isset($params["search_type"]) && isset($params["search_id"])) {

          $data["search_type"] = $params["search_type"];
          $data["search_id"] = $params["search_id"];
        }else{
          return null;
        }
        

        if (isset($params["currency_id"])) {
            $data["currency_id"] = $params["currency_id"];
        }

        if (isset($params["stars"])) {
          $data["stars"] = $params["stars"];
        }

        if (isset($params["categories"])) {
          $data["categories"] = $params["categories"];
        }

        return $data;
    }


    //REFACTORIZAR

    /*return array*/
    public static function getIdsFromResult($resultSet, $data, $type="room"){

        $begin = new \DateTime($data["checkin"], new \DateTimeZone('America/Caracas'));
        $end = new \DateTime($data["checkout"], new \DateTimeZone('America/Caracas'));

        $arIds = array();

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval, $end);

        //echo "$num_noches";

        $arDates = array();
        $bucle_id = null;
        $i=0;
        $len = count($resultSet);
        //4 VUELTAS - vuelta 1-->
        foreach ($resultSet as $item => $value) {
          if($type == "room"){
            if (($bucle_id == null || $bucle_id == $value->id))  { //SI ES LA PRIMERA VUELTA
              $bucle_id = $value->id;
              //GUARDO LAS FECHAS EN UN ARRAY PARA COMPARARLAS AL FINAL
              array_push($arDates, $value->fecha);
              if ($i == $len - 1) { //SI ES LA ULTIMA FECHA del arreglo
                $flag = true;
                foreach($daterange as $date){
                  if (!in_array($date->format("Y-m-d"), $arDates)) {
                    $flag = false;
                  }
                }
                if ($flag) {
                  if (!in_array($bucle_id, $arIds)) {
                    array_push($arIds, $bucle_id);
                  }
                }
              }
            }else{
              $flag = true;
              foreach($daterange as $date){
    
                if (!in_array($date->format("Y-m-d"), $arDates)) {
                  //echo "<br>tengo-->".$date->format("Y-m-d");
                  $flag = false;
                }
              }
              if ($flag) {
                if (!in_array($bucle_id, $arIds)) {
                  array_push($arIds, $bucle_id);
                }
              }
              $arDates = array();
              $bucle_id = $value->id;
              array_push($arDates, $value->fecha);
              //TERMINO LA COMPARACION DE UNA HABITACION
            }
          }elseif($type == "hotel"){
            if (($bucle_id == null || $bucle_id == $value->hotel_id))  { //SI ES LA PRIMERA VUELTA
              $bucle_id = $value->hotel_id;
              //GUARDO LAS FECHAS EN UN ARRAY PARA COMPARARLAS AL FINAL
              array_push($arDates, $value->fecha);
              if ($i == $len - 1) { //SI ES LA ULTIMA FECHA del arreglo
                $flag = true;
                foreach($daterange as $date){
                  if (!in_array($date->format("Y-m-d"), $arDates)) {
                    $flag = false;
                  }
                }
                if ($flag) {
                  if (!in_array($bucle_id, $arIds)) {
                    array_push($arIds, $bucle_id);
                  }
                }
              }
            }else{
              $flag = true;
              foreach($daterange as $date){
    
                if (!in_array($date->format("Y-m-d"), $arDates)) {
                  //echo "<br>tengo-->".$date->format("Y-m-d");
                  $flag = false;
                }
              }
              if ($flag) {
                if (!in_array($bucle_id, $arIds)) {
                  array_push($arIds, $bucle_id);
                }
                
              }
              $arDates = array();
              $bucle_id = $value->hotel_id;
              array_push($arDates, $value->fecha);
              //TERMINO LA COMPARACION DE UNA HABITACION
            }
          }
          
          $i++;
        }
        return $arIds;
    }
    public static function searchForAvailability($data){
        
        $begin = new \DateTime($data["checkin"], new \DateTimeZone('America/Caracas'));
        $end = new \DateTime($data["checkout"], new \DateTimeZone('America/Caracas'));

        $today = new \DateTime(null,  new \DateTimeZone('America/Caracas'));

        $hotel = null;
        $place = null;
        $city = null;
        $stars = null;
        $categories = null;

        $diff = $begin->diff($end);

        $diffFromToday = $today->diff($begin);

        //trace_log("hoy es:".$today->format("d-m-Y")."- diferencia es:".$diffFromToday->days);

        if (isset($data["stars"])) {
            $stars = explode("-", $data["stars"]);
        }

        if (isset($data["categories"])) {
          $categories = explode("-", $data["categories"]);
        }

        if (isset($data["search_type"]) && isset($data["search_id"]) ) {
          if ($data["search_type"]=="Lugares") {
            $place = $data["search_id"];
          }
          
          if ($data["search_type"]=="Hoteles") {
            $hotel = $data["search_id"];
          }

          if ($data["search_type"]=="Ciudades") {
            $city = $data["search_id"];
          }

        }

        $interval = new \DateInterval('P1D');
  
        $daterange = new \DatePeriod($begin, $interval, $end);
  
        $first=null;
  
        foreach($daterange as $date){
          $next = Db::table('hesperiaplugins_hoteles_fechas as a')
          //->select("a.disponible", "c.nombre", "c.id")
          ->select("c.id", "a.fecha", "a.disponible", "c.hotel_id as hotel_id")
          ->join("hesperiaplugins_hoteles_precios_fechas as b", "a.id", "=", "b.fecha_id" )
          ->join("hesperiaplugins_hoteles_moneda as d", "d.id", "=", "b.moneda_id" )
          ->join("hesperiaplugins_hoteles_habitaciones as c", "c.id", "=", "a.habitacion_id")
          ->join("hesperiaplugins_hoteles_hotel as f", "f.id", "=", "c.hotel_id")
          //->orderBy("c.id")
          ->where('fecha', "=", $date->format("Y-m-d"))
          ->where('d.id', "=", $data["currency_id"])
          ->where('c.status', "=", 1)
          ->where("f.minimo_noches", "<=", $diff->days)
          ->where("f.noches_antelacion", "<", $diffFromToday->days+1)
         // ->where('b.precio', ">", 0)
         
          ->when($hotel, function($query) use ($hotel){
            return $query->where('c.hotel_id', $hotel);
          })

          ->when($city, function($query) use ($city){
            return $query->where('f.city_id', $city);
          })

          ->when($place, function ($query) use ($place){
            return $query->join("qchsoft_hotelesextension_placeables as e", "e.placeable_id", "=", "c.id")
            ->where("e.place_id", "=", $place)
            ->where("e.placeable_type", "=",
            "HesperiaPlugins\\Hoteles\\Models\\Hotel");
          })

          ->when($city, function($query) use ($city){
            return $query->where('f.city_id', $city);
          })

          ->when($stars, function($query) use ($stars){
            return $query->whereIn('f.estrellas', $stars);
          })
          
          ->when($categories, function($query) use ($categories){
            return $query->whereIn('f.category_id', $categories);
          })

          //->where('c.hotel_id', $data["hotel"])
          ->where('a.disponible', ">", "0"); //nuevo para la otra comparacion

          if(isset($data["habitacion"]) && $data["habitacion"]!=""){
            $next->where("habitacion_id", "=", $data["habitacion"]);
          }
          if(isset($data["ocupacion"]) && $data["ocupacion"]!=""){
            $first->where("b.ocupacion", "=", $data["ocupacion"]);
          }
          if(isset($data["regimen"]) && $data["regimen"]!=""){
            $first->where("b.regimen_id", "=", $data["regimen"]);
          }
          if ($first==null)
            $first = $next;
          else
            $first= $first->union($next);
          //echo $date->format("Y-m-d") . "<br>";
        }
        if ($begin == $end) {
          $first = Db::table('hesperiaplugins_hoteles_fechas as a')
          //->select("a.disponible", "c.nombre", "c.id")
          ->select("c.id", "a.fecha", "a.disponible", "c.hotel_id as hotel_id")
          ->join("hesperiaplugins_hoteles_precios_fechas as b", "a.id", "=", "b.fecha_id" )
          ->join("hesperiaplugins_hoteles_moneda as d", "d.id", "=", "b.moneda_id" )
          ->join("hesperiaplugins_hoteles_habitaciones as c", "c.id", "=", "a.habitacion_id")
          ->join("hesperiaplugins_hoteles_hotel as f", "f.id", "=", "c.hotel_id")
          //->orderBy("c.id")
          ->where('fecha', "=", $begin->format("Y-m-d"))
          ->where('d.id', "=", $data["currency_id"])
          ->where('c.status', "=", 1)
          //->where('b.precio', ">", 0)
          ->where('c.hotel_id', $data["hotel"])
          ->where('a.disponible', ">", "0")

          ->where("f.minimo_noches", "<=", $diff->days)
          ->where("f.noches_antelacion", "<", $diffFromToday->days+1)

          ->when($hotel, function($query) use ($hotel){
            return $query->where('c.hotel_id', $hotel);
          })

          ->when($city, function($query) use ($city){
            return $query->where('f.city_id', $city);
          })

          ->when($place, function ($query) use ($place){
            return $query->join("qchsoft_hotelesextension_placeables as e", "e.placeable_id", "=", "c.id")
            ->where("e.place_id", "=", $place)
            ->where("e.placeable_type", "=",
            "HesperiaPlugins\\Hoteles\\Models\\Hotel");
          })

          ->when($stars, function($query) use ($stars){
            return $query->whereIn('f.estrellas', $stars);
          })

          ->when($categories, function($query) use ($categories){
            return $query->whereIn('f.category_id', $categories);
          });

  
          if(isset($data["habitacion"]) && $data["habitacion"]!=""){
            $first->where("habitacion_id", "=", $data["habitacion"]);
          }
          if(isset($data["ocupacion"]) && $data["ocupacion"]!=""){
            $first->where("b.ocupacion", "=", $data["ocupacion"]);
          }
          if(isset($data["regimen"]) && $data["regimen"]!=""){
            $first->where("b.regimen_id", "=", $data["regimen"]);
          }
        }


        $list = $first->orderBy("id")->get();

        return $list;
        /*
        recorremos las fechas para saber si las habitaciones estan disponibles
        en TODAS, asi poderlas devolver
        */
        
      }
      
      
      public static function applyMainFilter($query, $data){
        return $query;
      }
}