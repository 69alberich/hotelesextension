<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use Session;
use HesperiaPlugins\Hoteles\Models\Moneda;
class CurrencyHelper {

    public static function getCurrencyList(){
        //if(!Session::get("currency_list")){
            $list = Moneda::select("id", "moneda", "acronimo", "defecto", "tasa")->where("ind_activo", 1)->get();
            
            Session::put("currency_list", $list->toArray());
       // }

        return Session::get("currency_list");

    }
    #return model
    public static function getDefaultCurrency(){
        $obMoneda = Moneda::where("defecto", 1)->first();
        return $obMoneda;
    }


    public static function setSelectedCurrency($id){

        $currency = Moneda::select("id", "moneda", "acronimo", "defecto")->where("id", $id)->first();
        
        Session::put("current_currency", $currency->toArray());
    }

    public static function getCurrentCurrency(){

        if (!Session::get("current_currency")) {

            $currency = Moneda::select("id", "moneda", "acronimo", "defecto")->where("defecto", 1)->first();
        
            Session::put("current_currency", $currency->toArray());
        }

        return Session::get("current_currency");
    }
}