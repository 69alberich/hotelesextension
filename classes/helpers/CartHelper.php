<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use Db;
use Carbon\Carbon;

use Lovata\Toolbox\Classes\Helper\PriceHelper;


class CartHelper {
    

    public static function getRoomAvailability($room_id, $params){
        $begin = new Carbon($params["checkin"]);
        $end = new Carbon($params["checkout"]);
         
        //$end->subDay();
  
        $available = DB::table('hesperiaplugins_hoteles_fechas')
        ->whereBetween('fecha', [$begin->format("Y-m-d"), $end->format("Y-m-d")])
        ->where('habitacion_id', '=', $room_id)
        ->min('disponible');
  
        return $available;
      }
}