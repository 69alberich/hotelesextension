<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use Db;
use HesperiaPlugins\Hoteles\Models\Fecha;
use Carbon\Carbon;

use Lovata\Toolbox\Classes\Helper\PriceHelper;

class RoomPriceHelper {

    public static function prepareData($params){
        $data = array();

        if (isset($params["dates"])) {
            $arDates = explode(" to ", $params["dates"]);
            $data["checkin"] = $arDates[0];
            $data["checkout"] = $arDates[1];
        }

        if (isset($params["currency_id"])) {
            $data["currency_id"] = $params["currency_id"];
        }

        if (isset($params["search_type"]) && isset($params["search_id"])) {

          $data["search_type"] = $params["search_type"];
          $data["search_id"] = $params["search_id"];

        }

        return $data;
    }

    public static function getPricesArray($roomId, $params, $flag = true){
        //obtener los id de regimenes activos del hotel 
        $data = $params;

        $begin = new \DateTime($data["checkin"]);
        $end = new \DateTime($data["checkout"]);
        $end->sub(new \DateInterval('P1D'));
        $result = Db::table('hesperiaplugins_hoteles_fechas as a')
        ->select("b.ocupacion", "d.acronimo", "d.id as moneda_id", "b.regimen_id", "b.precio", 
        "c.nombre as regimen","a.fecha", "c.descripcion as detalle_regimen", "f.porcentaje_markup", "f.nombre as nombre_hotel",
        "f.id as hotel_id", "e.nombre as nombre_habitacion")
        ->join("hesperiaplugins_hoteles_precios_fechas as b", "a.id", "=", "b.fecha_id" )
        ->join("hesperiaplugins_hoteles_regimen as c", "b.regimen_id", "=", "c.id" )
        ->join("hesperiaplugins_hoteles_moneda as d", "b.moneda_id", "=", "d.id")
        ->join("hesperiaplugins_hoteles_habitaciones as e", "e.id", "=", "a.habitacion_id")
        ->join("hesperiaplugins_hoteles_hotel as f", "f.id", "=", "e.hotel_id")
        //->groupBy('b.ocupacion', 'b.regimen_id')
        ->whereBetween('a.fecha', [$begin->format("Y-m-d"), $end->format("Y-m-d")])
        ->where('a.habitacion_id', "=", $roomId)
        ->where('c.status', "=", 1)
        //->where('b.precio', ">", 0)
        ->where("d.id", "=", $data["currency_id"])
        ->when($flag, function ($query) {
            return $query->where("a.disponible", ">", 0);
        })
        //->whereIn('b.regimen_id', [1])
        ->orderBy('b.ocupacion')
        ->orderBy('regimen_id')->get();

        $arPaxRegimeFormatedPrices = self::formatByRegime($result);
        
        $arPrices["prices_by_pax_regime"] = $arPaxRegimeFormatedPrices;
        $arPrices["room_id"] = $roomId;
        $arPrices["min_price"] = self::getMinPrice($arPaxRegimeFormatedPrices);

        return $arPrices;
    }

    public static function getMinPrice($arPaxRegimeFormatedPrices){
        //var_dump($array);
      $min=0;
      $arMinPrice = array();

      foreach ($arPaxRegimeFormatedPrices as $key => $value) {

        if ($arPaxRegimeFormatedPrices instanceof Collection) {

          if ($value->total > 0 && $value->total < $min) {
            $min = $value->total;
            $arMinPrice = $value;

          }else if($value->total > 0 && $min == 0){
            $min = $value->total;
            $arMinPrice = $value;

          }
          //$moneda = $value->acronimo;
        }else{

          if ($value["total"] > 0 && $value["total"] < $min) {
            $min = $value["total"];
            $arMinPrice = $value;

          }else if($value["total"] > 0 && $min == 0){
            $min = $value["total"];
            $arMinPrice = $value;
          }
        }
      }

      return $arMinPrice;

    }
    public static function formatByRegime($result){
      
        $array = array();
        foreach ($result as $value) {

            $index = $value->ocupacion."-".$value->regimen_id;

            if (!array_key_exists($index, $array)) {
              //SI NO HA SIDO PROCESADO ESA OCUPACIO-REGIMEN
              $array[$index]["fechas"] = array();
              $array[$index]["nombre_regimen"] = $value->regimen;
              $array[$index]["total"] = 0;
              //$array[$index]["acronimo"] = $value->acronimo;
              $array[$index]["ocupacion"] = $value->ocupacion;
              $array[$index]["regimen_id"] = $value->regimen_id;
              $array[$index]["detalle_regimen"] = $value->detalle_regimen;
              $array[$index]["markup"] = $value->porcentaje_markup;
              $array[$index]["nombre_hotel"] = $value->nombre_hotel;
              $array[$index]["hotel_id"] = $value->hotel_id;
              $array[$index]["nombre_habitacion"] = $value->nombre_habitacion;
              
            }
            if (!in_array($value->fecha, $array[$index]["fechas"])) {
              array_push($array[$index]["fechas"], $value->fecha);
    
            }
            if ($value->precio > 0 && $array[$index]["total"] != -1) {
              $array[$index]["total"] = $array[$index]["total"] + $value->precio;
            }else{
              $array[$index]["total"] = -1;
            }
            //trace_log("precio:".$array[$index]["total"]." - markup:".$value->porcentaje_markup);
            $markup = $array[$index]["total"]*100/(100 - $value->porcentaje_markup);
            
            $array[$index]["total_markup"] = PriceHelper::round($markup);
          }
          return $array;
    }
    




}