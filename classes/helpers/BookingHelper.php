<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use RainLab\User\Models\User;
use RainLab\User\Models\UserGroup;

use HesperiaPlugins\Hoteles\Models\Reservacion;
use HesperiaPlugins\Hoteles\Models\Compra;
use HesperiaPlugins\Hoteles\Models\Upgrade;
use HesperiaPlugins\Hoteles\Models\DetalleReservacion;
use Auth;
use Db;
use Response;
use Carbon\Carbon;
use Session;
use Mail;

Class BookingHelper{

    private $cart;
    private $form;
    private $contactForm;
    private $bookingType;
    private $user;
    public $result;
    
    public function __construct($cart, $form){

        $this->cart = $cart;
        $this->form = $form;

        //trace_log($this->cart);

        $this->setBookingType();


        $this->setUser();

        $this->createReservation();

        $this->clearCart();

    }

    private function setBookingType(){
        $totalItems = count($this->cart["hotels"]) + count($this->cart["upsellings"]);

        if($totalItems == 1 && count($this->cart["hotels"]) > 0){
            $this->bookingType = "single";
        }elseif($totalItems > 0 || count($this->cart["upsellings"]) > 0){
            $this->bookingType = "multiple";
        }else{
          trace_log("carrito vacío");
        }
    }

    private function createReservation(){
        
        Db::transaction(function () {
            $today = Carbon::now();
            $expirationDate = $today->addDays(1);
            $sessionKey = uniqid('session_key', true);
            $sumPrices = 0;

            if($this->bookingType =="multiple"){
              $code = uniqid();
              $compra = Compra::create([
                "nombre_cliente" => $this->form["name"],
                "identificacion" => $this->form["document"],
                "comentario" => "",
                "status_id" => 2,
                "moneda_id" => 1,
                "origen_id" => 1,
                "contacto" => $this->form["contact"],
                "total" => 0,
                'usuario_id' => "-1",
                'fecha_vigencia' => $expirationDate,
                'codigo' => $code
                //"pago_insite"
              ]);

              $compra->usuario()->add($this->user);
            }
            foreach ($this->cart["hotels"] as $hotel) {
              $code = uniqid();
              $begin = new \DateTime($hotel["params"]["checkin"]);
              $end = new \DateTime($hotel["params"]["checkout"]);

              $reservacion = Reservacion::create([
                    'huesped' => $this->form["name"],
                    'checkin' => $begin->format("Y-m-d"),
                    'checkout' => $end->format("Y-m-d"),
                    'moneda_id' => $hotel["params"]["currency_id"],
                    'status_id' => 2,
                    'total' => 0,
                    'email' => $this->form["email"],
                    'identificacion' => $this->form["document"],
                    'contacto' => $this->form["contact"],
                    //'comentarios' => $data["comentarios"],
                    'hotel_id' => $hotel["hotel_id"],
                    'fecha_vigencia' => $expirationDate,
                    'origen_id' => 1,
                    'paquete_id' => 0,
                    'comentarios'=> "",
                    //'pago_insite' => 0,
                    'usuario_id' => "-1",
                    'codigo'=> $code,
                ]);

                $roomsTotalPice = 0;

                foreach ($hotel["rooms"] as $room) {

                  $resultPriceArray = $room["result_price_array"];
                  //$price = isset($room["upselling_id"]) ? $room["result_price_array"]["total_markup"] : $room["room_price_markup"]
                  $detalle = new DetalleReservacion([
                    'reservacion_id' => 0,
                    'habitacion_id' => $room["room_id"],
                    'ocupacion' => $room["result_price_array"]["ocupacion"],
                    'precio' => $room["result_price_array"]["total_markup"],
                    'regimen_id' => $room["result_price_array"]["regimen_id"],
                    'huespedes'=> "[]"
                  ]);

                  $roomsTotalPice = $resultPriceArray["total_markup"] +$roomsTotalPice;

                  $detalle->save();
                  if(isset($room["upselling_id"])){
                    $upgrade = new Upgrade([
                      'precio' => $resultPriceArray["upselling_markup_pax_price"],
                      'upgradable_type' => "HesperiaPlugins\Hoteles\Models\DetalleReservacion",
                      'upgradable_id' => $detalle->id,
                      'upselling_id' => $room["upselling_id"],
                      'moneda_id' => $hotel["params"]["currency_id"],
                      'cantidad' => 1,
                      'porcentaje_markup' => $room["result_price_array"]["upselling_markup"],
                      'ocupacion' => $room["result_price_array"]["ocupacion"],
                    ]);
                    
                    $detalle->upgrades()->add($upgrade);
                  }else{
                    //trace_log("no tengo upselling");
                  }

                  if(isset($room["tour_id"])){
                    $reservacion->paquete_id = $room["tour_id"];
                  }
                  $reservacion->detalles()->add($detalle, $sessionKey);
                  
                  $reservacion->total = $roomsTotalPice;
                }
                $sumPrices = $sumPrices + $roomsTotalPice;
                $reservacion->usuario()->add($this->user);

                $reservacion->save(null, $sessionKey);


                if($this->bookingType == "multiple"){
                  
                  
                  $compra->reservaciones()->add($reservacion);
                  
                  

                  
                }else{

                  $user = $this->user;
                  Mail::send("hesperiaplugins.hoteles::mail.pre_reserva",
                    $reservacion->getResumen(), function($message) use ($user) {
                      
                    $message->to($user->email);

                  });
                  $this->result = Response::json(
                    ["status" => "success", "message" => "OK",
                    "model" => "Reservacion", "code" => $reservacion->codigo]
                  ); 
                }
            }
            if($this->bookingType == "multiple"){

              if(count($this->cart["upsellings"]) > 0){
                

                foreach ($this->cart["upsellings"] as $upselling) {
                  $date = new \DateTime($upselling["date"]);
                  trace_log($upselling);
                  $upgrade = new Upgrade([
                    'precio' => $upselling["total_markup_price"],
                    'upselling_id' => $upselling["upselling_id"],
                    'moneda_id' => 1,
                    'cantidad' => 1,
                    'porcentaje_markup' => $upselling["markup"],
                    'fecha_disfrute' => $date->format("Y-m-d"),
                    'ocupacion' => $upselling["adults"]."-".$upselling["kids"],
                  ]);
                  
                  $compra->upgrades()->add($upgrade);
                  $sumPrices = $sumPrices + $upselling["total_markup_price"];
                }
                
                
                
              }

              $compra->total = $sumPrices;
              $compra->save(null, $sessionKey);
              
              Mail::send("hesperiaplugins.hoteles::mail.pre_compra",
                 $compra->getResumen(), function($message) use ($compra) {
                 $message->to($compra->usuario->email);

               });
               
              $this->result = Response::json(
                ["status" => "success", "message" => "OK",
                "model" => "Compra", "code" => $compra->codigo]
              ); 
            }
            
        
            //Db::table('posts')->delete();
        });

    }

    private function setUser(){
        $user = User::where("email", $this->form["email"])->first();
        if (!$user) {

            $pass = uniqid();
            //$group = UserGroup::find(3);

            $user = Auth::register([
            'name' => '',
            'email' => $this->form["email"],
            'password' => $pass,
            'password_confirmation' => $pass,
            ]);

            //$user->groups()->add($group);
        }

        $this->user = $user;
    }

    private function clearCart(){
      Session::forget("hotels");
      Session::forget("upsellings");
    }
    
}