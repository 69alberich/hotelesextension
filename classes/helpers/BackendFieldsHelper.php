<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use HesperiaPlugins\Hoteles\Models\Habitacion;
use HesperiaPlugins\Hoteles\Models\Regimen;
class BackendFieldsHelper {


    protected $room;
    protected $regime;
    
    public function __construct($roomId, $regimeId){
        $this->initRoom($roomId);
        $this->initRegime($regimeId);
    }

    public function initRoom($id){
        $this->room = Habitacion::find($id);
    }

    public function initRegime($id){
        $this->regime = Regimen::find($id);
    }

    public function makeConfig($formData, $adultsOnly = false){
        if (!$this->room) {
            return;
        }

        if (!$this->regime) {
            return;
        }

        $array = array();
        $limit = $this->room->capacidad;

        for ($i=1; $i <=$limit ; $i++) { 
            
            if(!$adultsOnly){
                $j = 0;
                while($i+$j <= $limit){
                    $ocupation = "$i"."-".$j;
                    
                    $paxArray = array();
                    $paxArray["code"] = $ocupation;
                    $paxArray["pax"] = $this->getLabel($ocupation);
                    $paxArray["regime"] = $this->regime->nombre;
                    if($formData["Fecha"]["price_type"]=="pax_type"){
                        $paxArray["price"] = $this->getPaxPrice($formData, $i, $j);
                    }else{
                        $paxArray["price"] = $this->getFullPrice($formData);
                    }
                    

                    array_push($array, $paxArray);
                    $j++;
                }
            }else{
                $paxArray = array();

                $ocupation = "$i"."-"."0";
                $paxArray["code"] = $ocupation;
                $paxArray["pax"] = $this->getLabel($ocupation);
                $paxArray["regime"] = $this->regime->nombre;
                if($formData["Fecha"]["price_type"]=="pax_type"){
                    $paxArray["price"] = $this->getPaxPrice($formData, $i, 0);
                }else{
                    $paxArray["price"] = $this->getFullPrice($formData);
                }
                
                array_push($array, $paxArray);
            }

            
        }

        return $array;
    }

    public function getLabel($ocupation){

        $aux = explode("-", $ocupation);
        $result = "$aux[0] Adultos - $aux[1] Niños";
        return $result;
    }

    public function getPaxPrice($formData, $adults, $childrens){
        $totalAdultPrice = $formData["Fecha"]["adult_price"]*$adults;

        $totalChildrenPrice = 0;
        
        if (isset($formData["Fecha"]["children_price"])) {
            $totalChildrenPrice = $formData["Fecha"]["children_price"]*$childrens;
        }
        return $totalAdultPrice+$totalChildrenPrice;
    }

    public function getFullPrice($formData){
        return $formData["Fecha"]["full_price"];
    }
}