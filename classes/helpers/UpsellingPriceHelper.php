<?php namespace Qchsoft\HotelesExtension\Classes\Helpers;

use Db;
use Carbon\Carbon;
use Lovata\Toolbox\Classes\Helper\PriceHelper;

class UpsellingPriceHelper {

    public static function getPricesArray($obUpselling, $params){
        $dates = $params["dates"];
        $currencyId = $params["currency_id"];


        $calendario_id = $obUpselling->calendario[0]->id;
        $first=null;
        $disponible = "";
        if ($obUpselling->tipo_inventario == 1) {
            $disponible = "a.disponible";
        }else{
            $disponible = "d.disponible";
        }
        if(is_array($dates)){
            //ES UN ARRAY DE FECHAS
            foreach($dates as $date){
            $date = new Carbon($date);
            $next = Db::table('hesperiaplugins_hoteles_fecha_calendario as a')
            ->select("a.fecha", "b.precio", $disponible, "b.precio_nino", "d.porcentaje_markup")
            ->join("hesperiaplugins_hoteles_precio_fecha_calendario as b", "a.id", "=", "b.fecha_id" )
            ->join("hesperiaplugins_hoteles_calendario as c", "c.id", "=", "a.calendario_id" )
            ->join("hesperiaplugins_hoteles_upselling as d", "d.id", "=", "c.calendarizable_id")
            ->where('a.fecha', "=", $date->format("Y-m-d"))
            ->where('b.moneda_id', "=", $currencyId)
            ->where("a.calendario_id", $calendario_id)
            ->where('c.calendarizable_type', "=", "HesperiaPlugins\\Hoteles\\Models\\Upselling");
            if ($first==null)
                $first = $next;
            else
                $first= $first->union($next);
            }
        }else{
            //ES UNA SOLA FECHA STRING
            $date = new Carbon($dates);
            $first = Db::table('hesperiaplugins_hoteles_fecha_calendario as a')
            ->select("a.fecha", "b.precio", $disponible, "b.precio_nino", "d.porcentaje_markup")
            ->join("hesperiaplugins_hoteles_precio_fecha_calendario as b", "a.id", "=", "b.fecha_id" )
            ->join("hesperiaplugins_hoteles_calendario as c", "c.id", "=", "a.calendario_id" )
            ->join("hesperiaplugins_hoteles_upselling as d", "d.id", "=", "c.calendarizable_id")
            ->where('a.fecha', "=", $date->format("Y-m-d"))
            ->where('b.moneda_id', "=", $currencyId)
            ->where("a.calendario_id", $calendario_id)
            ->where('c.calendarizable_type', "=", "HesperiaPlugins\\Hoteles\\Models\\Upselling");
        }

        $return = $first->orderBy("fecha")->get();

        return $return;
    }

}
