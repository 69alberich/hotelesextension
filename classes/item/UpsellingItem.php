<?php namespace QchSoft\HotelesExtension\Classes\Item;

use HesperiaPlugins\Hoteles\Models\Upselling;
use Qchsoft\HotelesExtension\Classes\Helpers\UpsellingPriceHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;

use Carbon\Carbon;

class UpsellingItem {

    private $obUpselling;

    
    function __construct($upsellingId, $obUpselling = null) {

        if ($upsellingId) {
            $this->obUpselling = Upselling::find($upsellingId);
        }else{
            $this->obUpselling = $obUpselling;
        }

    }

    function getUpsellingPrices($params){

        
        $obUpselling = $this->obUpselling;

        if($obUpselling->ind_calendario == 0){
            $calendarDates = $obUpselling->calendario[0]->fechas;
            $today = new Carbon();

            $firstAvailableDate = $calendarDates->where("fecha", ">=", $today->format("Y-m-d"))->first();

            //trace_log($firstAvailableDate);
            $carbonDate = new Carbon($firstAvailableDate->fecha);
            $params["dates"] = $carbonDate->format("Y-m-d");
            

        }
        $result = UpsellingPriceHelper::getPricesArray($obUpselling, $params);
        $kidsPrice = 0;
        $adultsPrice = 0;
        $markup = 0;
        $arResult = array();
        foreach ($result as $price ) {
            $adultsPrice = $adultsPrice + $price->precio;
            $kidsPrice = $kidsPrice + $price->precio_nino;
            $markup = $price->porcentaje_markup;   
        }

        $arResult["upselling_name"] = $obUpselling->titulo;
        if($obUpselling->hotel != null){
            $arResult["hotel_name"] = $obUpselling->hotel->nombre;
            $arResult["hotel_id"] = $obUpselling->hotel->id;
        }
        
        $arResult["upselling_id"] = $obUpselling->id;
        $arResult["adults_price"] =  $adultsPrice;
        $arResult["adults_price_markup"] = round($adultsPrice*100/(100 - $markup), 2);
        $arResult["kids_price"] = $kidsPrice;
        $arResult["kids_price_markup"] = round($kidsPrice*100/(100 - $markup), 2);
        $arResult["markup"] = $markup;

        return $arResult;

    }
    public function getUpselling(){
        return $this->obUpselling;
    }

}
