<?php namespace QchSoft\HotelesExtension\Classes\Item;

use HesperiaPlugins\Hoteles\Models\Hotel;
use HesperiaPlugins\Hoteles\Models\Habitacion;
use Qchsoft\HotelesExtension\Classes\Helpers\SearchHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\RoomPriceHelper;

class HotelResult {

    protected $resultSet;
    protected $params;
    protected $id;
    protected $arAttributes;
    protected $rooms;
    protected $defaultRegimeMinPrice;
    protected $model;
    protected $minPrice;
    public $category;

    function __construct($id, $resultSet, $params){
        $this->id = $id;
        $this->resultSet = $resultSet;
        $this->params = $params;

        $this->init();
    }


    public function init(){

        $this->model = Hotel::find($this->id);

        foreach ($this->model->indexable as $key => $value) {
          $this->arAttributes[$value] = $this->model->$value;             
        }

        $this->loadRoomsAvailability();
        $this->setDefaultRegimeMinPrice();

        $this->setHotelMinPrice();
        
        $this->category = $this->model->category;
    }

    private function loadRoomsAvailability(){

        $hotelRoomIds = $this->model->habitaciones->modelKeys();

        $arRoomIdFromResult = SearchHelper::getIdsFromResult($this->resultSet, $this->params, "room");
        
        $availableRooms = array_where($hotelRoomIds, function ($value, $key) use ($arRoomIdFromResult) {
            return in_array($value, $arRoomIdFromResult);
        });
        
    
        $arrayPrices = array();

        foreach ($availableRooms as $id ) {
            $arrayPrices[$id] = RoomPriceHelper::getPricesArray($id, $this->params);        
        }
        
        $this->rooms = $arrayPrices;

    }

    public function setDefaultRegimeMinPrice(){
        //trace_log("entro !!");
        $defaultRegime = $this->model->getRegimenPorDefecto();

        $min = 0;
        $arMinPrice = null;

        foreach ($this->rooms as $key => $room) {
            $arPricesByPaxRegime = $room["prices_by_pax_regime"];

            foreach ($arPricesByPaxRegime as $keyPrice => $price) {
              
              $arPaxRegimeCode = explode("-", $keyPrice);
             //trace_log($arPaxRegimeCode);
              if ($arPaxRegimeCode[2] == $defaultRegime->id) {
                
                //trace_log("tengo:".$defaultRegime->nombre);
                if ($price["total"] > 0 && $price["total"] < $min) {
                    $min = $price["total"];
                    $arMinPrice = $price;
        
                  }else if($price["total"] > 0 && $min == 0){
                    $min = $price["total"];
                    $arMinPrice = $price;
        
                  }
              }
            }
        }

       $this->defaultRegimeMinPrice =  $arMinPrice;

    }

    public function setHotelMinPrice(){
        $min = 0;

        foreach ($this->rooms as $key => $room) {
        
            if ($room["min_price"] > 0 && $room["min_price"] < $min) {

                $min = $room["min_price"];
    
            }else if($room["min_price"] > 0 && $min == 0){
                $min = $room["min_price"];
           
            }
        }
            
        $this->minPrice = $min;
    }
    public function getModel(){
        return $this->model;
    }

    public function getAttributes(){
        return $this->arAttributes;
    }

    public function getMinPrice(){
        return $this->minPrice;
    }

    public function defaultRegimeMinPrice(){

        return $this->defaultRegimeMinPrice;
    }

}