<?php namespace QchSoft\HotelesExtension\Classes\Item;

use HesperiaPlugins\Hoteles\Models\Paquete;
use Qchsoft\HotelesExtension\Classes\Helpers\RoomPriceHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\UpsellingPriceHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;

use Carbon\Carbon;

class TourItem {

    private $obPaquete;
    public  $startDate;
    public $endDate;
    
    function __construct($paqueteId, $obPaquete = null) {

        if ($paqueteId) {
            $this->obPaquete = Paquete::find($paqueteId);
        }else{
            $this->obPaquete = $obPaquete;
        }
        $this->startDate =  new Carbon($this->obPaquete->fecha_desde);
        $this->endDate = new Carbon($this->obPaquete->fecha_hasta);
    }

    function getNightsDescription(){
        $description = "";

        if($this->obPaquete->fecha_desde != null && $this->obPaquete->fecha_hasta != null){
            $startDate =  new Carbon($this->obPaquete->fecha_desde);
            $endDate = new Carbon($this->obPaquete->fecha_hasta);

            $diff = $startDate->diffInDays($endDate);
            $description = $diff." Días y ".($diff-1)." Noches";

        }

        return $description;
    }

    function getTourPrices(){
        $currency = CurrencyHelper::getCurrentCurrency();
        $obUpselling = $this->obPaquete->upsellings[0];
        $params = [
            "dates" => [$this->obPaquete->fecha_desde],
            "checkin" => $this->startDate->format("d-m-Y"),
            "checkout" => $this->endDate->format("d-m-Y"),
            "currency_id" => $currency["id"]
        ];

       $result = UpsellingPriceHelper::getPricesArray($obUpselling, $params);
       $result->toArray();
       
       $arRoomPrices = $this->getRoomPrices();
       $arTourPrices = array();
       foreach ($arRoomPrices as $key => $value) {

            $pax = explode("-", $value["ocupacion"]);
            $totalPrice = ($pax[0]*$result[0]->precio)+($pax[1]*$result[0]->precio_nino);

            $value["tour_name"] = $this->obPaquete->titulo;
            $value["tour_id"] = $this->obPaquete->id;
            $value["upselling_pax_price"] = $totalPrice;
            $value["upselling_markup_pax_price"] = round($totalPrice*100/(100 - $result[0]->porcentaje_markup), 2);
            $value["upselling_markup"] = $result[0]->porcentaje_markup;
            $value["room_price"] = $value["total"];
            $value["room_price_markup"] = $value["total_markup"];
            $value["total_markup"] = $value["room_price_markup"]+$value["upselling_markup_pax_price"];

            
            $arTourPrices[$key] = $value;
       }
       return $arTourPrices;
       //trace_log($arTourPrices);
    }

    function getRoomPrices(){

        
        $currency = CurrencyHelper::getCurrentCurrency();

        $arrayPrices = array();
        $params = [
            "checkin" => $this->startDate->format("d-m-Y"),
            "checkout" => $this->endDate->format("d-m-Y"),
            "currency_id" => $currency["id"]
        ];

        $room = $this->getRoom();

        $resultPrices = RoomPriceHelper::getPricesArray($room->id, $params);  

        $array = array_where($resultPrices["prices_by_pax_regime"], function ($value, $key) use ($room) {
            
            if($value["regimen_id"] == $room->pivot["regimen_id"]){
                return $value;
            }
           
            
        });

        return $array;

        //return $arrayPrices;

    }
    
    public function getRoom(){
        return $this->obPaquete->habitaciones[0];
    }

    public function getUpselling(){
        return $this->obPaquete->upsellings[0];
    }

}
