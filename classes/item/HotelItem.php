<?php namespace QchSoft\HotelesExtension\Classes\Item;


use Lovata\Toolbox\Classes\Item\ElementItem;

use HesperiaPlugins\Hoteles\Models\Hotel;
//use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
//use Lovata\Shopaholic\Classes\Collection\OfferCollection;

class HotelItem extends ElementItem{
    const MODEL_CLASS = Hotel::class;
   

    public static $arQueryWith = [
        'banner',
        'foto_inicio',
        'galeria',
        'habitaciones',
    ];

    /** @var Hotel */
    protected $obElement = null;

    /*public $arRelationList = [
        'offer'               => [
            'class' => OfferCollection::class,
            'field' => 'offer_id_list',
        ],
        'category'            => [
            'class' => CategoryItem::class,
            'field' => 'category_id',
        ],
        'additional_category' => [
            'class' => CategoryCollection::class,
            'field' => 'additional_category_id',
        ],
        'brand'               => [
            'class' => BrandItem::class,
            'field' => 'brand_id',
        ],
    ];*/

    /**
     * Check element, active == true, trashed == false
     * @return bool
     */
    public function isActive()
    {
        return $this->active && !$this->trashed;
    }

    /*
    protected function getElementData()
    {
        $arResult = [
            'offer_id_list'          => $this->obElement->offer->where('active', true)->pluck('id')->all(),
            'additional_category_id' => $this->obElement->additional_category->pluck('id')->all(),
            'trashed'                => $this->obElement->trashed(),
        ];

        foreach ($this->obElement->offer as $obOffer) {
            OfferItem::make($obOffer->id, $obOffer);
        }

        return $arResult;
    }*/
}
