<?php namespace Qchsoft\HotelesExtension\Classes\Event;

use HesperiaPlugins\Hoteles\Models\Habitacion as HabitacionModel;
use HesperiaPlugins\Hoteles\Controllers\Habitaciones as HabitacionesController;

class HabitacionesControllerHandler {

    public function subscribe($obEvent){


        HabitacionesController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form

            

            if (!$model instanceof HabitacionModel) {
                return;
            }
            
            if($form->isNested) {
                return;
            }

            $form->addFields([
                'content' => [        
                    'type'  => 'partial',
                    'path' => '$/qchsoft/hotelesextension/partials/_habitacion_button_bar.htm',
                    'context' => ['update', 'preview']
                ],
            ]);

            $form->addTabFields([
                'atributos' => [
                    'tab' => 'Atributos',
                    'type'  => 'partial',
                    'path' => '$/hesperiaplugins/hoteles/controllers/habitaciones/_atributos_list.htm',
                    'context' => ['update', 'preview']
                ],
            ]);
            

        });
        
        
    }
    

}