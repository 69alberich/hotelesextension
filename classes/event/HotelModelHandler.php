<?php namespace Qchsoft\HotelesExtension\Classes\Event;

use HesperiaPlugins\Hoteles\Models\Hotel as HotelModel;
use QchSoft\Location\Models\City;
use QchSoft\HotelesExtension\Models\Place;
use QchSoft\HotelesExtension\Models\Category;
class HotelModelHandler{

    public function subscribe(){

        HotelModel::extend(function($model) {
            $model->belongsTo['city'] = [City::class,
             'key' => 'city_id'];

             $model->morphToMany["places"] =  [Place::class,
              'name' => 'placeable', "table"=> "qchsoft_hotelesextension_placeables"];

            $model->belongsTo['category'] = [Category::class,
            'key' => 'category_id'];

            $model->belongsToMany['additional_categories'] = [
                '\Qchsoft\HotelesExtension\Models\Category',
                 'table' => 'qchsoft_hotelesextension_hotel_extra_categories'
            ];
        });
          
    }

}
