<?php namespace Qchsoft\HotelesExtension\Classes\Event;

use HesperiaPlugins\Hoteles\Models\Hotel as HotelModel;
use HesperiaPlugins\Hoteles\Controllers\Hotel as HotelController;

class HotelControllerHandler {

    public function subscribe($obEvent){

        HotelController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof HotelModel) {
               
                return;
            }
           
            if($form->isNested === false) {
                $form->addTabFields([
                    'city' => [
                        'label' => 'Ciudad',
                        'tab' => 'Caracteristicas',
                        'type'  => 'relation',
                        'nameFrom' => "name",
                        'span' => "left"
                    ],
                    'minimo_noches' => [
                        'label' => 'Minimo de noches',
                        'tab' => 'Configuración',
                        'type'  => 'number',
                        'span' => "right"
                    ],
                    'noches_antelacion' => [
                        'label' => 'Noches de antelación',
                        'tab' => 'Configuración',
                        'type'  => 'number',
                        'span' => "left"
                    ],
                    'orden' => [
                        'label' => 'Orden',
                        'tab' => 'Configuración',
                        'type'  => 'number',
                        'span' => "left"
                    ],
                    'estrellas' => [
                        'label' => 'Estrellas',
                        'tab' => 'Configuración',
                        'type'  => 'number',
                        'span' => "right"
                    ],
    
                    'category' => [
                        'label' => 'Categoría',
                        'tab' => 'Caracteristicas',
                        'type'  => 'relation',
                        'nameFrom' => "name",
                        'span' => "right"
                    ],
    
                    'places' => [
                        'tab' => 'Lugares',
                        'type'  => 'partial',
                        'path' => '$/qchsoft/hotelesextension/partials/_hotels_places_relation.htm',
                        'context' => ['update', 'preview']
                    ],
                    
                    'additional_categories' => [
                        'tab' => 'Categorias Adicionales',
                        'type'  => 'partial',
                        'path' => '$/qchsoft/hotelesextension/partials/_hotels_categories_relation.htm',
                        'context' => ['update', 'preview']
                    ],
                ]);
            }

        });

        HotelController::extendListColumns(function($list, $model){
            if (!$model instanceof HotelModel) {
                return;
            }
        
            /*
            $list->addColumns([
                'created_at' => [
                    'label' => 'lovata.toolbox::lang.field.created_at',
                    'sortable' => true,
                    'invisible' => true,
                    'type' => "datetime",
                    'format' => "d-m-Y | h:i:s A"
                ],
                'updated_at' => [
                    'label' => 'lovata.toolbox::lang.field.updated_at',
                    'sortable' => true,
                    'invisible' => true,
                    'type' => "datetime",
                    'format' => "d-m-Y | h:i:s A"
                ],
                'currency' =>[
                    'label' => 'Currency',
                    'relation' => 'currency',
                    'select' => 'code'
                ]
            ]);*/
        });


        HotelController::extend(function($controller) {
          

            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }
        
            // Splice in configuration safely
            $myConfigPath = '$/qchsoft/hotelesextension/config/hotel_relation_config.yaml';

            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );

            $configListCustomPath = '$/qchsoft/hotelesextension/config/hotel_list_extended_config.yaml';

            $controller->listConfig = $controller->mergeConfig(
                $controller->listConfig,
                $configListCustomPath
            );
        });
        
    }
    

}