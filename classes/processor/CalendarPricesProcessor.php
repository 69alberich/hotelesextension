<?php namespace Qchsoft\HotelesExtension\Classes\Processor;

use HesperiaPlugins\Hoteles\Models\Habitacion;
use \hesperiaplugins\Hoteles\Models\Fecha as FechaModel;
use \hesperiaplugins\Hoteles\Models\PrecioFecha as PrecioFechaModel;
use Db;
class CalendarPricesProcessor {


    protected $room;
    protected $datesData;
    protected $generatedPrices;
    
    protected $beginDate;
    protected $endDate;
    protected $quantity;

    public function __construct($roomId){
        $this->initRoom($roomId);
    }

    public function initRoom($id){
        $this->room = Habitacion::find($id);
    }

    public function setDatesData($data){
        $this->datesData = $data;
    }

    public function setGeneratedPrices($tableData){
        $aux = json_decode($tableData);
        $this->generatedPrices = $aux;
    }
    
    public function generateDates($form){
        //$this->init($form);
        $this->prepareData($form);
        $this->setGeneratedPrices($form["pricesTableData"]);

        $interval = new \DateInterval('P1D');

        $daterange = new \DatePeriod($this->beginDate, $interval, $this->endDate);
        //trace_log($form);
        foreach($daterange as $date){
            //if(in_array($date->format("N"), $form["days_of_week"])){

                $date = FechaModel::firstOrNew([
                    "habitacion_id" => $this->room->id,
                    "fecha" => $date->format("Y-m-d")
                ]);

                $date->disponible = $this->quantity;
                $date->save();
                
                foreach ($this->generatedPrices as $price) {
                    $datePrice = PrecioFechaModel::FirstOrNew([
                        "fecha_id" => $date->id,
                        "regimen_id" => $form["Fecha"]["regimen"],
                        "moneda_id" => $form["Fecha"]["moneda"],
    
                        "ocupacion" => $price->code
                    ]);
        
                    $datePrice->precio = $price->price;
                    $datePrice->save();
                }
                
        }   
    }

    public function updateAvailability($form){
        $this->prepareData($form);
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($this->beginDate, $interval, $this->endDate);

        DB::table('hesperiaplugins_hoteles_fechas')
          ->where("habitacion_id", "=", $this->room->id)
          ->whereBetween('fecha', [
            $this->beginDate->format("Y-m-d"),
            $this->endDate->format("Y-m-d")])
          ->update(['disponible' =>$this->quantity]);
    }

    public function prepareData($form){

        $this->beginDate = new \DateTime($form["Fecha"]["fecha_desde"]);
        $this->endDate = new \DateTime($form["Fecha"]["fecha_final"]);
        $this->quantity = $form["Fecha"]["cantidad"];

    }
}