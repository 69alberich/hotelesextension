<?php namespace Qchsoft\HotelesExtension\Components;

use Cms\Classes\ComponentBase;
use HesperiaPlugins\Hoteles\Models\Reservacion;
use Input;

class ReservationPage extends ComponentBase{
    

    public function defineProperties(){
        return [
            'slug' => [
                'slug' => 'slug',
                'description' => 'url for search',
                'type' => 'string',
                'required'=> true
            ]
        ];

    }

    public function componentDetails(){
        return [
          'name'=> 'Reservation Page',
          'description' => 'Component for render reservacion model in page'
        ];
    }
    
    

    public function getReservation(){

        $properties = $this->getProperties();
        $reservation = Reservacion::where("codigo", $properties["slug"])->first();

        return $reservation;

    }

}

    