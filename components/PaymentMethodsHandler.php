<?php namespace Qchsoft\HotelesExtension\Components;

use Cms\Classes\ComponentBase;
use Input;
use HesperiaPlugins\Hoteles\Models\TipoPago;
use HesperiaPlugins\Hoteles\Models\Pago;
use HesperiaPlugins\Hoteles\Models\Compra;
use HesperiaPlugins\Hoteles\Models\Reservacion;
use QchSoft\HotelesExtension\Models\Settings;

use Response;
use Mail;


class PaymentMethodsHandler extends ComponentBase{

    private $paymentMethodList;    

    public function defineProperties(){
        return [];

    }

    public function componentDetails(){
        return [
          'name'=> 'Payment Methods handler',
          'description' => 'methods for payment methods'
        ];
      }


    public function getPaymentMethodsList(){
        return TipoPago::active()->get();
    }


    public function onCreatePayment(){
      $data = Input::post();
      $response = null;
      trace_log($data);
      //$file = Input::file("archivo2");
      //$data["archivo"] = Input::file("archivo2");

      if($data["type"] == "compra"){

        $obObject = Compra::find($data["id"]);

      }elseif($data["type"] == "reservacion"){

        $obObject = Reservacion::find($data["id"]);

      }else{
        return false;
      }

      try {
        
        $obPago = Pago::create([
          "referencia" => $data["reference"],
          "pagable_type" => $obObject::class,
          "tipo_pago_id" => $data["payment_method_id"],
          "pagable_id" => $obObject->id,
        ]);
            
        $obPago->save();
        
        $obObject->status_id = $data["status_id"];
  
        $obObject->save();

        $emailList = Settings::get("email_list");

        $arEmailList = explode("," , $emailList);

        

        if($data["payment_method_id"] == 1){ //SI ES STRIPE SE APRUEBA DE UNA

          $obObject->aprobar(null, $arNotificaciones = $arEmailList);
        
        }

        $response = Response::json(['status' => 'success', 'message' => 'Good']);

      } catch (\Throwable $th) {
        //throw $th;

        $response = Response::json(['status' => 'error', 'message' => 'bad']);
        
        trace_log($th);
      }
      
      return $response;
    }

}