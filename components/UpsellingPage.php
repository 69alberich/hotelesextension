<?php namespace Qchsoft\HotelesExtension\Components;

use Cms\Classes\ComponentBase;
use HesperiaPlugins\Hoteles\Models\Upselling;
use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\UpsellingPriceHelper;
use QchSoft\HotelesExtension\Classes\Item\UpsellingItem;
use Carbon\Carbon;

class UpsellingPage extends ComponentBase{

  public $upselling;

  public $maxAdults = 5;
  public $maxKids = 5;

  public function defineProperties(){
    return [
      'slug' => [
          'title'       => 'Slug',
          'description' => 'tour slug',
          'default'     => '{{ :slug }}',
          'type'        => 'string'
      ],
    ];
  }

  public function componentDetails(){
    return [
      'name'=> 'Upselling page',
      'description' => ''
    ];
  }

  public function onRender(){
    //$this->moneda_inicial = $this->property("moneda");
  }
  public function onRun(){
   
    $this->upselling  = Upselling::where("slug", $this->param('slug'))->first();

  }


  public function onLoadPrices(){
    //trace_log(post());
    $currency = CurrencyHelper::getCurrentCurrency();
    $params = post();
    
    $params["currency_id"] = $currency["id"];
    
    $upsellingItem = new UpsellingItem($params["upselling_id"], null);

    $arResult = $upsellingItem->getUpsellingPrices($params);

    //$result->toArray();
    //$this->page["arPrices"] = $result;
  
    $this->page["arPrices"] = $arResult;
  }


  public function loadSinglePrices($upsellingId){
    $currency = CurrencyHelper::getCurrentCurrency();
    
    $params = array();

    $params["upselling_id"] = $upsellingId;
    $params["currency_id"] = $currency["id"];
    $params["dates"] = null;

    $upsellingItem = new UpsellingItem($params["upselling_id"], null);

    $arResult = $upsellingItem->getUpsellingPrices($params);

    return($arResult);

  }

}
