<?php namespace Qchsoft\HotelesExtension\Components;

use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\RoomPriceHelper;

use Cms\Classes\ComponentBase;
use HesperiaPlugins\Hoteles\Models\Hotel;
use HesperiaPlugins\Hoteles\Models\Regimen;
use Input;
use Redirect;
class HotelPage extends ComponentBase{
    
    public $searchParams;
    public $roomNights = 0;

    public function defineProperties(){
        return [
            'slug' => [
                'slug' => 'slug',
                'description' => 'url for search',
                'type' => 'string',
                'required'=> true
            ]
        ];

    }

    public function componentDetails(){
        return [
          'name'=> 'Hotel Page',
          'description' => 'Component for render hotel model in page'
        ];
    }
    
    

    public function onRun(){
  
        $data = Input::all();
        $currency = CurrencyHelper::getCurrentCurrency();

        $data["currency_id"] = $currency["id"];
            
        $this->searchParams = $data;
            
        $this->page["searchParams"] = $this->searchParams;
        
        $this->setRoomNights();
    }
    public function getHotel(){

        $properties = $this->getProperties();

        $hotel = Hotel::where("slug", $properties["slug"])->first();

        return $hotel;

    }

    public function setRoomNights(){
        if (isset($this->searchParams["dates"])) {

            $arDates = explode(" to ", $this->searchParams["dates"]);

            if (isset($arDates[0]) && isset($arDates[1])) {

                $begin = new \DateTime($arDates[0], new \DateTimeZone('America/Caracas'));
                $end = new \DateTime($arDates[1], new \DateTimeZone('America/Caracas'));

                $diff = $begin->diff($end);

                if ($diff->days > 0) {
                    $this->roomNights = $diff->days;
                }
            }
            
        }
    }

    public function getRoomPrices($roomId){

        $params = $this->searchParams;
        $currency = CurrencyHelper::getCurrentCurrency();

        $params["currency_id"] = $currency["id"];

        $data = RoomPriceHelper::prepareData($params);

        $result = RoomPriceHelper::getPricesArray($roomId, $data, true);

        return $result;

    }


    public function onLoadRoomPrices(){

        $params = Input::post();

        $currency = CurrencyHelper::getCurrentCurrency();

        $params["currency_id"] = $currency["id"];

        $data = RoomPriceHelper::prepareData($params);
        
        $result = RoomPriceHelper::getPricesArray($params["room_id"], $data, true);

        $this->page["regimenList"] = $this->getRegimeFromPricesArray($result);
        $this->page["paxList"] = $this->getPaxFromPricesArray($result);
        $this->page["prices"] = $result["prices_by_pax_regime"];
        $this->page["room_id"] = $params["room_id"];

    }


    private function getRegimeFromPricesArray($arrayPrices){
        $regimeList = array();

        foreach ($arrayPrices["prices_by_pax_regime"] as $price) {
           $arRegime = array();
           $regime = Regimen::find($price["regimen_id"]);
           $arRegime["id"] = $price["regimen_id"];
           $arRegime["name"] = $price["nombre_regimen"];
           $arRegime["description"] = $price["detalle_regimen"];
           $arRegime["icon"] = $regime->icon;
           
           $flag = true;

           if ($flag == true && count($regimeList) == 0) {
            
                array_push($regimeList,$arRegime);

           }else{

                for ($i=0; $i < count($regimeList); $i++) { 
            
        
                    if ($arRegime["id"] == $regimeList[$i]["id"]) {
                        $flag = false;
                    }
                }
                
                if ($flag) {
                    array_push($regimeList,$arRegime);
                }
           }
          
        }

        return $regimeList;
    }

    private function getPaxFromPricesArray($arrayPrices){
        $paxList = array();

        foreach ($arrayPrices["prices_by_pax_regime"] as $price) {
            $arPax = array();
 
            $arPax["pax"] = $price["ocupacion"];
            $pax = explode("-", $price["ocupacion"]);
            $paxDescription = "$pax[0] Adultos - $pax[1] Niños";
            $arPax["pax_description"] = $paxDescription;
            
            if (!in_array($arPax, $paxList)) {
                array_push($paxList, $arPax);
            }
 
         }

         return $paxList;
    }

    public function onChangeDates(){

        
        $params = Input::post();
        
        $dates = str_replace(" ", "+", $params["dates"]);

        return Redirect::to("hotel/".$params["slug"]."?dates=".$dates."#hotel-dates-form");
    }

}

    