<?php namespace Qchsoft\HotelesExtension\Components;

use Cms\Classes\ComponentBase;
use HesperiaPlugins\Hoteles\Models\Compra;
use Input;

class PurchasePage extends ComponentBase{
    

    public function defineProperties(){
        return [
            'slug' => [
                'slug' => 'slug',
                'description' => 'url for search',
                'type' => 'string',
                'required'=> true
            ]
        ];

    }

    public function componentDetails(){
        return [
          'name'=> 'Purchase Page',
          'description' => 'Component for render compra model in page'
        ];
    }
    
    

    public function getPurchase(){

        $properties = $this->getProperties();
        $compra = Compra::where("codigo", $properties["slug"])->first();

        return $compra;

    }

}

    