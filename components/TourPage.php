<?php namespace Qchsoft\HotelesExtension\Components;

use Cms\Classes\ComponentBase;
use HesperiaPlugins\Hoteles\Models\Paquete;

use QchSoft\HotelesExtension\Classes\Item\TourItem;
use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;

use Carbon\Carbon;

class TourPage extends ComponentBase{

  public $tour;

  public $maxAdults;
  public $maxKids;
  public $room;
  public $upselling;

  public function defineProperties(){
    return [
      'slug' => [
          'title'       => 'Slug',
          'description' => 'tour slug',
          'default'     => '{{ :slug }}',
          'type'        => 'string'
      ],
    ];
  }

  public function componentDetails(){
    return [
      'name'=> 'Tour page',
      'description' => ''
    ];
  }

  public function onRender(){
    //$this->moneda_inicial = $this->property("moneda");
  }
  public function onRun(){
   
    $this->tour  = Paquete::where("slug", $this->param('slug'))->first();

    $this->room = $this->tour->habitaciones[0];

    $this->maxAdults = $this->room->capacidad;
    $this->maxKids = $this->room->capacidad - 1;

  }



  public function loadItem(){
    
    $item = new TourItem(null, $this->tour);

    return $item;
  }

}
