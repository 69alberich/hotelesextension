<?php namespace Qchsoft\HotelesExtension\Components;

use Qchsoft\HotelesExtension\Classes\Helpers\SearchHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\RoomPriceHelper;
use Qchsoft\HotelesExtension\Classes\Item\HotelItem;

use HesperiaPlugins\Hoteles\Models\Hotel;

use Cms\Classes\ComponentBase;
use Redirect;
use Input;
class SearchHandler extends ComponentBase{

    public $filters;
    public $searchParams;

    protected $suggestions;
    protected $hotelList;
    protected $count = 0;
    

    public function defineProperties(){
        return [];

    }

    public function componentDetails(){
        return [
          'name'=> 'Search handler',
          'description' => 'methods for search and autocomplete'
        ];
      }

    public function onRun(){
  
        $data = Input::all();

        $page = 1;

        if (isset($data["page"])) {
            $page = $data["page"];
        }

        $currency = CurrencyHelper::getCurrentCurrency();

        $data["currency_id"] = $currency["id"];
        
        $response = SearchHelper::getAvailableHotels($data);

        if ($response["list"] != null) {

            $resultList = $response["list"];

            $sorted = $resultList->sortBy(function ($hotel, $key) use ($data) {
                $sort = "default";

                if (isset($data["sort"])) {
                    $sort = $data["sort"];
                }
                $arAttributes = $hotel->getAttributes();
                switch ($sort) {

                    case 'min-price':
                        $minPrice = $hotel->getMinPrice();
                        return $minPrice["total"];
                        break;

                    default:
                        return $arAttributes["orden"];
                        break;
                }
               
            });
            $sorted->values()->all();
    
            $this->count = $sorted->count();
            $this->searchParams = $data;
            $this->hotelList = $sorted->forPage($page, 10);
            $this->filters = $response["filters"];
            $this->page["searchParams"] = $this->searchParams;
        }
        
    }

    public function getCount(){
        return $this->count;
    }

    public function onAutoComplete(){
        $data = post();
        return SearchHelper::getSuggestions();
    }

    public function onSearch(){
        $data = post();
        $data["page"]=1;
        return Redirect::to('busqueda/1/q?'.http_build_query($data));
    }

    public function getHotelsFromRequest(){
       
       return $this->hotelList;

    }

    public function getRoomPrices($roomId){
        $data = Input::all();
        $currency = CurrencyHelper::getCurrentCurrency();

        $data["currency_id"] = $currency["id"];

        $result = RoomPriceHelper::getPricesArray($roomId, $data, true);

        return $result;
    }

    public function getSearchParamUrl(){

        if (isset($this->searchParams["page"])) {
            unset($this->searchParams["page"]); 
        }
        return http_build_query($this->searchParams);    
    }

    public function getDatesFromUrl(){
        $array = array();
        //echo "tengo:".$this->searchParams["dates"];

        $array["dates"] = $this->searchParams["dates"];

        return http_build_query($array);
    }

    public function onFilter(){
        
        $data = Input::post();
        
        if (isset($data["stars"]) && is_array($data["stars"])) {
            $data["stars"] = implode("-", $data["stars"]);
        }

        if (isset($data["categories"]) && is_array($data["categories"])) {
            $data["categories"] = implode("-", $data["categories"]);
        }
        
        return Redirect::to('busqueda/1/q?'.http_build_query($data));
    }

}