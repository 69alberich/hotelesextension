<?php namespace Qchsoft\HotelesExtension\Components;

use Lovata\Toolbox\Classes\Helper\PriceHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;
use Cms\Classes\ComponentBase;
use Redirect;
use Input;

class CurrencyHandler extends ComponentBase{

    private $currencyList;    

    public function defineProperties(){
        return [];

    }

    public function componentDetails(){
        return [
          'name'=> 'Currency handler',
          'description' => 'methods for currency actions'
        ];
      }

      public function onRun(){
  
        $this->currencyList = CurrencyHelper::getCurrencyList();
        
        
    }


    public function getCurrencyList(){
        return  CurrencyHelper::getCurrencyList();
    }

    public function getDefaultCurrency(){
      
        return CurrencyHelper::getDefaultCurrency();
        
    }

    public function getCurrentCurrency(){
        return CurrencyHelper::getCurrentCurrency();
    }

    public function onChangeCurrency(){
        $data = post();

        CurrencyHelper::setSelectedCurrency($data["id"]);

    }

    public function getEquivalentArray($mount){
        $defaultCurrency = CurrencyHelper::getDefaultCurrency();
        $currencyList = CurrencyHelper::getCurrencyList();

        $arEquivalent = array();

        foreach ($currencyList as $currency) {
            $arEquivalent[$currency["acronimo"]] = PriceHelper::round($mount*$currency["tasa"]);
            
        }

        return $arEquivalent;
    }

}