<?php namespace Qchsoft\HotelesExtension\Components;

use Qchsoft\HotelesExtension\Classes\Helpers\CurrencyHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\RoomPriceHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\CartHelper;
use Qchsoft\HotelesExtension\Classes\Helpers\BookingHelper;
use QchSoft\HotelesExtension\Classes\Item\TourItem;
use QchSoft\HotelesExtension\Classes\Item\UpsellingItem;
use Cms\Classes\ComponentBase;
use Input;
use Session;
use Response;
use Flash;
use Carbon\Carbon;

class CartHandler extends ComponentBase{

    private $cartCount = 0;

    public function defineProperties(){
        return [];

    }

    public function componentDetails(){
        return [
          'name'=> 'Cart Handler',
          'description' => 'Component for cart operations'
        ];
    }

    public function onAddRoom(){
        //Session::forget("hotels");
        $params = post();
        $data = RoomPriceHelper::prepareData($params);
        
        //EVAL ROOM AVAILABILITY 

        if(!$this->evalAvailability($params["room_id"], $data)){
            //trace_log("no tengo");
            return false;
        }

        
        $paxRegimeCode = $params["pax"]."-".$params["regime"];

        $currency = CurrencyHelper::getCurrentCurrency();

        $data["currency_id"] = $currency["id"];
        $data["quantity"] = 1;
        $data["room_id"] = $params["room_id"];

        $result = RoomPriceHelper::getPricesArray($params["room_id"], $data, true);

        $pricesByPaxRegime = $result["prices_by_pax_regime"];

        $selected = $pricesByPaxRegime[$paxRegimeCode];

        if($selected["total_markup"] > 0){
            $cartHotelPosition = $this->createCartHotel($selected, $data);

            $cartHotelPosition["params"] = $data;
        
        
            $this->updateCart($cartHotelPosition, $data);
        }else{
            Flash::error("No se pudo añadir su selección");
        }   
      
    

    }

    private function evalAvailability($room_id, $params){

        $arHotels = Session::get("hotels");
        $roomQuantity = 0;

        if($arHotels){
            foreach ($arHotels as $hotel) {
                foreach ($hotel["rooms"] as $rooms) {
                    if ($rooms["room_id"] == $room_id) {
                        $roomQuantity++;
                    }
                }
            }
        }
        
        $available = CartHelper::getRoomAvailability($room_id, $params);

        //trace_log("disponible:".($available - $roomQuantity));

        if ($available - $roomQuantity > 0) {
            return true;
        }else{
            return false;
        }
    }

    private function updateCart($cartHotelPosition, $params){
        //trace_log("update aca");

        $arHotels = Session::get("hotels");
        $flagAdded = false;
        if ($arHotels != null) {
            
            foreach ($arHotels as $index => $hotelPosition) {
               
                if($hotelPosition["hotel_id"] == $cartHotelPosition["hotel_id"] && 
                    $hotelPosition["checkin"] == $params["checkin"] && 
                    $hotelPosition["checkout"] == $params["checkout"]){
                   
                    $rooms = $cartHotelPosition["rooms"];

                    foreach ($rooms as $room) {
                        array_push($hotelPosition["rooms"], $room);
                    }

                    $arHotels[$index] = $hotelPosition;
                    $flagAdded = true;
                }       
            }

            if(!$flagAdded){
                array_push($arHotels, $cartHotelPosition);
            }
            
            
        }else{
            
            $arHotels = array();
            array_push($arHotels, $cartHotelPosition);
        }
        Session::put("hotels", $arHotels);
    }
    public function onRemove(){
        $params = Input::post();

        if($params["type"] == "room"){
            $arHotels = Session::get("hotels");

            foreach ($arHotels as $hotelIndex => $hotel) {
            if ($hotel["hotel_id"] == $params["hotel_id"]) {
                
                    foreach ($hotel["rooms"] as $roomIndex => $room) {
                        //trace_log("tengo:".$roomIndex." y recibo:".$params["room_index"]);
                        if ($roomIndex == $params["room_index"]) {
                            array_splice($hotel["rooms"], $roomIndex, 1);
                            //array_keys($hotel["rooms"]);
                        }
                    }

                    $arHotels[$hotelIndex] = $hotel;
            }

            if(count($hotel["rooms"]) < 1){
                array_splice($arHotels, $hotelIndex, 1);
                //array_keys($arHotels);
            }
            }
            
            Session::put("hotels", $arHotels);
        }elseif($params["type"] == "upselling"){
            $arUpsellings = Session::get("upsellings");

            foreach ($arUpsellings as $index => $upselling) {
                if($params["index"] == $index){
                    array_splice($arUpsellings, $index, 1);
                }
            }
            Session::put("upsellings", $arUpsellings);
        }else{
            return false;
        }
        
    }
    
    public function onClear(){

    }

    private function createCartHotel($priceArray, $params){
        
        if ($priceArray == null) {
            return;
        }

        $cartPosition = array();

        $cartPosition["hotel_id"] = $priceArray["hotel_id"];
        $cartPosition["hotel_name"] = $priceArray["nombre_hotel"];
        $cartPosition["checkin"] = $params["checkin"];
        $cartPosition["checkout"] = $params["checkout"];
        $cartPosition["hotel_markup"] = $priceArray["markup"];

        $pax = explode("-", $priceArray["ocupacion"]);

        $paxDescription = "$pax[0] Adultos - $pax[1] Niños";
        
        $cartPosition["rooms"] = array();
        
        $room =[
            "room_id" => $params["room_id"],
           
            "room_name" => $priceArray["nombre_habitacion"],
            "regime_name" => $priceArray["nombre_regimen"],
            "params" => $params,
            "price" => $priceArray["total_markup"],
            "pax_description" => $paxDescription,
            "result_price_array" => $priceArray
        ];
        if(isset($priceArray["tour_name"])){
            $room["tour_name"] = $priceArray["tour_name"];
            $room["tour_id"] = $priceArray["tour_id"];
            $room["upselling_id"] = $params["upselling_id"];
        }
        array_push($cartPosition["rooms"], $room);

        return $cartPosition;

    }


    public function getCart(){
        $arCart = array();
        $arCart["hotels"] = Session::get("hotels");
        $arCart["upsellings"] = Session::get("upsellings");
        
        return $arCart;
        
    }

    public function getCartPriceArray(){

        $arHotels = Session::get("hotels");
        $arUpsellings = Session::get("upsellings");

        $arTotalPrices = array();

        $totalPrice = 0;
        if($arHotels != null){
            foreach ($arHotels as $hotel) {
            
                foreach ($hotel["rooms"] as $room) {
    
                    $totalPrice = $room["price"]+$totalPrice;
                }
                
            }
        }
        if($arUpsellings != null){
            foreach ($arUpsellings as $upselling) {
                $totalPrice = $upselling["total_markup_price"]+$totalPrice;
            }
        }
        
        
        $arTotalPrices["total_price"] = $totalPrice;

        $currencyList = CurrencyHelper::getCurrencyList();
        foreach ($currencyList  as $currency) {
            $arTotalPrices[$currency["acronimo"]] = $totalPrice*$currency["tasa"];
        }
        //$arTotalPrices["total_raw"] =      
        
        return $arTotalPrices;
    }

    /*
    public function getSecundaryMount($mount, $currencyCode){
        $defaultCurrency = CurrencyHelper::instance()->getDefault();
       
        if($currencyCode == "USD"){
           $convertedPrice = $mount*$defaultCurrency->rate;
       }else{
           $convertedPrice = $mount/$defaultCurrency->rate; 
       }
       
       return PriceHelper::format($convertedPrice) ;
    }
    
    */

    public function getCartCount(){

        $arHotels = Session::get("hotels");
        $arUpsellings = Session::get("upsellings");

        if($arHotels != null){
            foreach ($arHotels as $hotel) {
            
                foreach ($hotel["rooms"] as $room) {
    
                    $this->cartCount++;
                }
                
            }
        }
        if ($arUpsellings != null ) {
           $this->cartCount = $this->cartCount + count($arUpsellings);
        }

        return $this->cartCount;
    }

    public function onBook(){

        $data = Input::post();
        $arHotels = array();
        $arUpsellings = array();

        if(Session::get("hotels") != null){
            $arHotels = Session::get("hotels");
        }
        if(Session::get("upsellings") != null){
            $arUpsellings = Session::get("upsellings");
        }
        
        $totalItems = 0;

        /*
        if($arHotels == null && $arUpsellings == null){
            return Response::json(['status' => 'error', 'message' => 'empty cart']); 
        }else{
            
        }*/
        $totalItems = count($arHotels) + count($arUpsellings);
        $cart = [
            "hotels" => $arHotels,
            "upsellings" => $arUpsellings
        ];
        if ($totalItems > 0) {
            $data["document"] = $data["document_type"]."-".$data["document"];
            $data["contact"] = $data["country_code"]."-".$data["phone"];
            $bookingHelper = new BookingHelper($cart, $data);
            trace_log($bookingHelper->result);
            return $bookingHelper->result;

        }else{
            trace_log("no paso");
            return Response::json(['status' => 'error', 'message' => 'empty cart']); 
        }
        
    }

    public function onAddTour(){
        $params = post();
        $pax = $params["adults"]."-".$params["kids"];
        $currency = CurrencyHelper::getCurrentCurrency();
        $tour =  new TourItem($params["tour_id"], null);

        $upselling = $tour->getUpselling();
        $room = $tour->getRoom();

        $dates = [
            "checkin" => $tour->startDate,
            "checkout" => $tour->endDate,
        ];

        
        $arPrices = $tour->getTourPrices();
        $selected = null; 
        foreach ($arPrices as $price) {
            if($price["ocupacion"] == $pax)
                $selected = $price;
        }
        $data = [];

        $data["currency_id"] = $currency["id"];
        $data["quantity"] = 1;
        $data["room_id"] = $room->id;
        $data["upselling_id"] = $upselling->id;
        $data["checkin"] = $tour->startDate->format("d-m-Y");
        $data["checkout"] = $tour->endDate->format("d-m-Y");
        
        for ($i=0; $i < $params["quantity"]; $i++) { 
            $cartHotelPosition = $this->createCartHotel($selected, $data);

            $cartHotelPosition["params"] = $data;

            $this->updateCart($cartHotelPosition, $data);
        }
        
        /*
        NO OLVIDAR
        $available = CartHelper::getRoomAvailability($room->id, $dates);
        if($available <= $params["quantity"]){
            
        }
        */
    }

    public function onAddUpselling(){

        $params = post();

        /*if($params["dates"] == null || $params["dates"] ==""){
            return false;
        }
        */

        
        $arCartUpselling = Session::get("upsellings");
        if ($arCartUpselling == null)  {
            $arCartUpselling = array();
        }
        $currency = CurrencyHelper::getCurrentCurrency();
        
      

        $params["currency_id"] = $currency["id"];
        
        $upsellingItem = new UpsellingItem($params["upselling_id"], null);

        $obUpselling = $upsellingItem->getUpselling();

        if($obUpselling->ind_calendario == 0){
            $calendarDates = $obUpselling->calendario[0]->fechas;
            $today = new Carbon();

            $firstAvailableDate = $calendarDates->where("fecha", ">=", $today->format("Y-m-d"))->first();

            //trace_log($firstAvailableDate);
            $carbonDate = new Carbon($firstAvailableDate->fecha);
            $params["dates"] = $carbonDate->format("d-m-Y");
            

        }

        $arResult = $upsellingItem->getUpsellingPrices($params);
        //var totalPrice = parseFloat(parseInt($("#kid-select").val())*parseFloat(kidsPrice))+(parseInt($("#adult-select").val())*parseFloat(adultsPrice));
        $totalMarkupPrice = ($params["kids"]*$arResult["kids_price_markup"])+($params["adults"]*$arResult["adults_price_markup"]);
        $totalPrice = ($params["kids"]*$arResult["kids_price"])+($params["adults"]*$arResult["adults_price"]);

        
        $upsellingCartItem = array();

        $upsellingCartItem["upselling_name"] = $obUpselling->titulo;
        $upsellingCartItem["upselling_id"] = $obUpselling->id;
        $upsellingCartItem["date"] = $params["dates"];
        $upsellingCartItem["adults"] = $params["adults"];
        $upsellingCartItem["kids"] = $params["kids"];
        $upsellingCartItem["total_markup_price"] = $totalMarkupPrice;
        $upsellingCartItem["total_price"] = $totalPrice;
        $upsellingCartItem["markup"] = $arResult["markup"];
        $upsellingCartItem["pax_description"] = $params["adults"]." Adultos - ".$params["kids"]." Niños";

        for ($i=0; $i < $params["quantity"]; $i++) { 
            array_push($arCartUpselling, $upsellingCartItem);
        }
        //array_push($arCartUpselling, $upsellingCartItem);

        Session::put("upsellings", $arCartUpselling);

        //trace_log($data);
    }

}