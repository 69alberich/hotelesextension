<?php namespace QchSoft\HotelesExtension\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use Db;
use Redirect;
use Response;

use \Hesperiaplugins\Hoteles\Models\Fecha as FechaModel;
use \hesperiaplugins\Hoteles\Models\PrecioFecha;
use Hesperiaplugins\Hoteles\Models\Habitacion;

use Qchsoft\HotelesExtension\Classes\Helpers\BackendFieldsHelper;
use Qchsoft\HotelesExtension\Classes\Processor\CalendarPricesProcessor;
use Validator;
use ValidationException;

class Calendar extends Controller
{
    public $implement = [
      'Backend\Behaviors\ListController',
      'Backend\Behaviors\FormController',
      /*'Backend\Behaviors\ReorderController',
    'Backend\Behaviors\RelationController'*/];

    public $listConfig = 'config_list.yaml';

    //public $listConfig = "config_list_fechas.yaml";
    public $formConfig = 'config_form.yaml';
   // public $reorderConfig = 'config_reorder.yaml';
    //public $relationConfig = 'config_relation.yaml';

    private $habitacion;

    
    public function __construct(){
        
      parent::__construct();
      BackendMenu::setContext('HesperiaPlugins.Hoteles', 'main-menu-item', 'side-menu-item4');

      $this->habitacion = Habitacion::find($this->params[0]);

      $this->addJs('/plugins/qchsoft/hotelesextension/assets/fullcalendar/lib/main.js');
      $this->addJs('/plugins/qchsoft/hotelesextension/assets/js/custom-calendar.js');
      $this->addCss('/plugins/qchsoft/hotelesextension/assets/fullcalendar/lib/main.css');
      $this->addCss('/plugins/qchsoft/hotelesextension/assets/fullcalendar/css/custom-calendar.css');
   
    }

    public function getHabitacion(){
      return $this->habitacion;
    }
    
    public function onLoadDatePrices(){
      $data = post();
      
      
      $prices = $this->habitacion->getPrecios($data["startDate"], $data["endDate"], 1, true);

      foreach ($prices as $index => $price) {

        $prices[$index]["ocupacion_descripcion"] = $this->habitacion->getOcupacion($price["ocupacion"]);
      }

      $this->vars["prices"] = $prices;

      return [
        '#modalPricesBody' => $this->makePartial('modal_prices_body')
      ];

    }
    public function onGeneratePrices(){
      $data = post();

      $messages = [
        'required' => ' :attribute es requerido.',
        'after' => 'Fecha hasta debe ser despues de la  Fecha antes',
        
      ];

      if ($data["Fecha"]["price_type"]== "pax_type") {
        $rules = [
          'fecha_desde' => 'required',
              'fecha_final' => 'required|after:fecha_desde',
              'regimen' => 'required',
              'cantidad' => 'required',
              'adult_price' => 'required|numeric',
        ];
      }elseif($data["Fecha"]["price_type"] == "full_type"){
        $rules = [
          'fecha_desde' => 'required',
          'fecha_final' => 'required|after:fecha_desde',
          'regimen' => 'required',
          'cantidad' => 'required',
          'full_price' => 'required|numeric',
        ];
      }
      
      $validator = Validator::make(
          $data["Fecha"],
          $rules,
          $messages
      );

      if ($validator->fails()) {
      throw new ValidationException($validator);
      }

      $backendFieldHelper = new BackendFieldsHelper($this->habitacion->id, $data["Fecha"]["regimen"]); 
      $generatedPrices = $backendFieldHelper->makeConfig($data, $this->habitacion->soloAdultos());

      $arrayConfig = [
        "fields" => [
          "prices" => [
            "type" => "datatable",
            "adding" => false,
            "deleting" => false,
            "columns" => [
              "code" => [
                "type" => "string",
                "title" => "Codigo",
                "readOnly" => true
              ],
              "pax" => [
                "type" => "string",
                "title" => "Ocupacion",
                "readOnly" => true
              ],
              "regime" => [
                "type" => "string",
                "title" => "Regimen",
                "readOnly" => true
              ],
              "price" => [
                "type" => "string",
                "title" => "Precio",
              ],
            ],
            "default"=> $generatedPrices
          ]
        ]
      ];
      $config = $this->makeConfigFromArray($arrayConfig);
      $config->model = new FechaModel();
    
      $widget = $this->makeWidget("Backend\Widgets\Form", $config);
      $this->vars["datatable"] = $widget;

      return [
        '#datatable-container' => $this->makePartial('datatable')
      ];
    }
    public function onLoadDateQuantity(){
      $data = post();
      $roomId = $this->habitacion->id;
      $begin = new \DateTime($data["startDate"]);
      $end = new \DateTime($data["endDate"]);
      $interval = new \DateInterval('P1D');
      $daterange = new \DatePeriod($begin, $interval, $end);

      $collection = Db::table('hesperiaplugins_hoteles_fechas as a')
      ->select("a.disponible as quantity", "a.fecha as date")
      ->where("habitacion_id", $roomId)
      ->whereBetween('fecha', [
        $begin->format("Y-m-d"),
        $end->format("Y-m-d")])->get();

      //trace_log($collection->toArray());
      return Response::json(['status' => 'success', 'value' => $collection->toArray()]);
    }
    
    public function listExtendQuery($query){
    if ($this->habitacion)
        $query->where('habitacion_id', $this->habitacion->id);
    }


    public function create($habitacion_id, $context = null){
      
      return $this->asExtension('FormController')->create($context);

    }
    
    public function availability($habitacion_id, $context = null){
      $config = $this->makeConfig("$/qchsoft/hotelesextension/config/availability_fields.yaml");
      $config->model = new FechaModel();
    
      $widget = $this->makeWidget("Backend\Widgets\Form", $config);
      $this->vars["form"] = $widget;

      
    }

    public function create_onSave($id=null){
      $data = post();

      $calendarPriceProcessor = new CalendarPricesProcessor($this->habitacion->id);
      $calendarPriceProcessor->generateDates($data);

      Flash::success("Proceso Completado");

    }

    public function availability_onSave(){
      $data = post();

      $calendarPriceProcessor = new CalendarPricesProcessor($this->habitacion->id);

      $array = [
        "Fecha"=> $data
      ];

      $calendarPriceProcessor->updateAvailability($array);

      Flash::success("Proceso Completado");
      
    }
    
  
    public function formExtendFieldsBefore($widget){

      $regimenes = Db::table('hesperiaplugins_hoteles_regimen as a')
      ->join("hesperiaplugins_hoteles_hotel_regimen as b", "a.id", "=", "b.regimen_id")
      ->where('b.hotel_id', '=', $this->habitacion->hotel->id)->lists('a.nombre', 'a.id');
     

      

      $widget->fields["adult_price"] =[
        "label" => "Precio por adulto",
        "span" => "left",
        "required" => 1,
        "type" => "number",
        "trigger" => [
          "action" => "show",
          "field" => "price_type",
          "condition" => "value[pax_type]"
        ]
      ];

      if(!$this->habitacion->soloAdultos()){
        $widget->fields["children_price"] =[
          "label" => "Precio por niño",
          "span" => "right",
          "required" => 1,
          "type" => "number",
          "trigger" => [
            "action" => "show",
            "field" => "price_type",
            "condition" => "value[pax_type]"
          ]
        ];
      }

      $widget->fields["regimen"]= [
        "label" => "Regimen",
        "type" => "dropdown",
        "span" => "auto",
        "options" => $regimenes
      ];

      $widget->fields["action_block"] =[
        "type" => "partial",
        "path" => "$/qchsoft/hotelesextension/controllers/calendar/_generate_button.htm"
      ];
    }

    
    /*
    
    
    */
}
